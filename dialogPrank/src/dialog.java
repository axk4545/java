
/* This program is meant as a harmless prank to to shutdown a computer. Do not abuse it.
 * As of now the only way to end execution of the program is through a task manager/killer
 *  
 *   Copyright (C) 2013  Aidan Kahrs
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
	
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class dialog{

		public static void main(String[] args){
		//custom title, error icon

		JFrame frame = new JFrame("");							//create dialog components
		      frame.setDefaultCloseOperation(2);
		      frame.pack();
		      frame.setVisible(true);

		JOptionPane.showMessageDialog(frame,					//shows dialog to shutdown
		    "Your computer has decided it does not like you\n"
		     +"and will now shutdown.",
		    "System Error",
		    JOptionPane.ERROR_MESSAGE);
		    
		    String shutdownCommand;								//shutdown commands
		    String operatingSystem = System.getProperty("os.name");

		    if (operatingSystem.contains("Linux") || operatingSystem.contains("Mac")) {
		        shutdownCommand = "shutdown -h now";
		    }
		    else if (operatingSystem.contains("Windows")) {
		        shutdownCommand = "shutdown.exe -s -t 0";
		    }
		    else {
		        throw new RuntimeException("Unsupported operating system.");
		    }

		    try {
				Runtime.getRuntime().exec(shutdownCommand);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    System.exit(0);
		}
}


