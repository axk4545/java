This program is meant as a harmless prank to to shutdown a computer. Do not abuse it.
As of now the only way to end execution of the program is through a task manager/killer.
The sourcecode is included in the file dialog.java. It can also be obtained from the Git repository at https://github.com/abkahrs/java.git
